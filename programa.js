

var ctx;
var canvas;
var palabra;
var letras = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZ";
var colorTecla = "black";
var colorMargen = "#558AEC";
var inicioX = 200;
var inicioY = 300;
var lon = 35;
var margen = 20;

var teclas_array = new Array();
var letras_array = new Array();
var palabras_array = new Array();

var aciertos = 0;
var errores = 0;

palabras_array.push("FUNCION");
palabras_array.push("OBJETO");
palabras_array.push("CLASE");
palabras_array.push("METODO");
palabras_array.push("VARIABLE");
palabras_array.push("REPOSITORIO");
palabras_array.push("ETIQUETA");
palabras_array.push("JAVASCRIPT");
palabras_array.push("PARAMETRO");
palabras_array.push("DECISION");
palabras_array.push("ARREGLO");
palabras_array.push("OPERACION");
palabras_array.push("ATRIBUTO");
palabras_array.push("CANVAS");
palabras_array.push("HTML");
        
function Tecla(x, y, ancho, alto, letra){
    this.x = x;
    this.y = y;
    this.ancho = ancho;
    this.alto = alto;
    this.letra = letra;
    this.dibuja = dibujaTecla;
}

function Letra(x, y, ancho, alto, letra){
    this.x = x;
    this.y = y;
    this.ancho = ancho;
    this.alto = alto;
    this.letra = letra;
    this.dibuja = dibujaCajaLetra;
    this.dibujaLetra = dibujaLetraLetra;
}

function dibujaTecla(){

    ctx.fillStyle = colorTecla;
    ctx.strokeStyle = colorMargen;
    ctx.fillRect(this.x, this.y, this.ancho, this.alto);
    ctx.strokeRect(this.x, this.y, this.ancho, this.alto);
    
    ctx.fillStyle = "white";
    ctx.font = "bold 20px Courier";
    ctx.fillText(this.letra, this.x+this.ancho/2-5, this.y+this.alto/2+5);
}

function dibujaLetraLetra(){
    var w = this.ancho;
    var h = this.alto;
    ctx.fillStyle = "black";
    ctx.font = "bold 40px Times New Roman";
    ctx.fillText(this.letra, this.x+w/2-12, this.y+h/2+14);
}
function dibujaCajaLetra(){
    ctx.fillStyle = "white";
    ctx.strokeStyle = "black";
    ctx.fillRect(this.x, this.y, this.ancho, this.alto);
    ctx.strokeRect(this.x, this.y, this.ancho, this.alto);
}

function pistaFunction(palabra){
    let pista = ""; 
    switch(palabra){   
        case 'FUNCION':   
            pista = "Conjunto de sentencias agrupadas, que cumplen un objetivo.";
            break;     
        case 'OBJETO':
            pista = "Una instancia de una Clase.";
            break;
        case 'CLASE':
            pista = "Define las características del Objeto.";
            break;
        case 'METODO':
            pista = "Una capacidad del Objeto.";
            break;
        case 'VARIABLE':
            pista = "Elemento que se emplea para almacenar un valor.";
            break;
        case 'REPOSITORIO':
            pista = "Lugar donde el código de un programa está almacenado.";
                break;  
        case 'ETIQUETA':
            pista = "Estructura o elemento del lenguaje de programación HTML";
             break;
        case 'JAVASCRIPT':
            pista = "Lenguaje de programación.";
             break;  
         case 'PARAMETRO':
            pista = "Variable que requiere una función cuando se llama.";
            break;   
        case 'DECISION':
            pista = "Estructuras de control que realizan una pregunta la cual retorna verdadero o falso";
             break;  
        case 'ARREGLO':
            pista = "Conjunto de datos o una estructura de datos.";
            break;
        case 'OPERACION':
            pista = "Existen légicas, aritméticas, asignación y comparación.";
            break;  
         case 'ATRIBUTO':
            pista = "Variables de un objeto.";
             break;  
        case 'CANVAS':
            pista = "Espacio para dibujar gráficas en 2D y 3D.";
            break; 
        case 'HTML':
            pista = "Lenguaje de marcado que se utiliza para el desarrollo de páginas de Internet";
            break;                          
        default:  
            pista="No hay pista aun";
    }

    ctx.fillStyle = "gray";  
    ctx.font = "20px Times New Roman";  
    ctx.fillText(pista, 20, 20);  
}


function teclado(){
    var ren = 0;
    var col = 0;
    var letra = "";
    var miLetra;
    var x = inicioX;
    var y = inicioY;
    for(var i = 0; i < letras.length; i++){
        letra = letras.substr(i,1);
        miLetra = new Tecla(x, y, lon, lon, letra);
        miLetra.dibuja();
        teclas_array.push(miLetra);
        x += lon + margen;
        col++;
        if(col==10){
            col = 0;
            ren++;
            if(ren==2){
                x = 280;
            } else {
                x = inicioX;
            }
        }
        y = inicioY + ren * 50;
    }
}

function pintaPalabra(){
    var p = Math.floor(Math.random()*palabras_array.length);
    palabra = palabras_array[p];

    pistaFunction(palabra);

    var w = canvas.width;
    var len = palabra.length;
    var ren = 0;
    var col = 0;
    var y = 230;
    var lon = 50;
    var x = (w - (lon+margen) *len)/2;
    for(var i=0; i<palabra.length; i++){
        letra = palabra.substr(i,1);
        miLetra = new Letra(x, y, lon, lon, letra);
        miLetra.dibuja();
        letras_array.push(miLetra);
        x += lon + margen;
    }
}

function horca(errores){
    var imagen = new Image();
    imagen.src = "imagenes/ahorcado"+errores+".png";
    imagen.onload = function(){
        ctx.drawImage(imagen, 390, 0, 230, 230);
    }
    /*************************************************
    // Imagen 2 mas pequeña a un lado de la horca //       
    var imagen = new Image();
    imagen.src = "imagenes/ahorcado"+errores+".png";
    imagen.onload = function(){
        ctx.drawImage(imagen, 620, 0, 100, 100);
    }
    *************************************************/
}

function ajusta(xx, yy){
    var posCanvas = canvas.getBoundingClientRect();
    var x = xx-posCanvas.left;
    var y = yy-posCanvas.top;
    return{x:x, y:y}
}

function selecciona(e){
    var pos = ajusta(e.clientX, e.clientY);
    var x = pos.x;
    var y = pos.y;
    var tecla;
    var bandera = false;
    for (var i = 0; i < teclas_array.length; i++){
        tecla = teclas_array[i];
        if (tecla.x > 0){
            if ((x > tecla.x) && (x < tecla.x + tecla.ancho) && (y > tecla.y) && (y < tecla.y + tecla.alto)){
                break;
            }
        }
    }
    if (i < teclas_array.length){
        for (var i = 0 ; i < palabra.length ; i++){ 
            letra = palabra.substr(i, 1);
            if (letra == tecla.letra){ 
                caja = letras_array[i];
                caja.dibujaLetra();
                aciertos++;
                bandera = true;
            }
        }
        if (bandera == false){ 
            errores++;
            horca(errores);
            if (errores == 5) gameOver(errores);
        }
        
        ctx.clearRect(tecla.x - 1, tecla.y - 1, tecla.ancho + 2, tecla.alto + 2);
        tecla.x - 1;
        
        if (aciertos == palabra.length) gameOver(errores);
    }
}


function gameOver(errores){
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.fillStyle = "black";

    ctx.font = "50px Times New Roman";
    if (errores < 5){
        ctx.fillText("¡Felicidades!, la palabra es: ", 110, 280);
    } else {
        ctx.fillText("Lo sentimos, la palabra era: ", 110, 280);
    }
    
    ctx.font = "bold 80px Times New Roman";
    lon = (canvas.width - (palabra.length*48))/2;
    ctx.fillText(palabra, lon, 380);
    horca(errores);
}

window.onload = function(){
    canvas = document.getElementById("pantalla");
    if (canvas && canvas.getContext){
        ctx = canvas.getContext("2d");
        if(ctx){
            teclado();
            pintaPalabra();
            horca(errores);
            canvas.addEventListener("click", selecciona, false);
        } else {
            alert ("Error al cargar el contexto!");
        }
    }
}
